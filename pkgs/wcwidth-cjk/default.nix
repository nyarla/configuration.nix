{ stdenv, lib, fetchFromGitHub, autoreconfHook}:

stdenv.mkDerivation rec {
  name    = "wcwidth-cjk-${src.rev}";
  src     = fetchFromGitHub {
    owner   = "fumiyas";
    repo    = "wcwidth-cjk";
    rev     = "e4a40fc0ed5977b740171f78b9092f458c71fee8";
    sha256  = "0k7n5a6dhm27bhycsif78xy76s82i3iw993mg48zcr18vc4mq57m";
  }; 

  meta = with stdenv.lib; {
    description = "Run command with CJK-friendly wcwidth(3) to fix ambiguous width chars";
    homepage    = "https://github.com/fumiyas/wcwidth-cjk";
    license     = stdenv.lib.licenses.bsd2;
    maintainers = [ "nyarla <nyarla@thotep.net>" ];
  };

  buildInputs = [ autoreconfHook ];

  patches = [] ++ (lib.optional stdenv.isDarwin ./patches/osx.patch) ;

  preAutoConf = ''
    mkdir m4
  '';
}


{ stdenv, fetchurl }:
stdenv.mkDerivation rec {
  version = "v0.5.1";
  name    = "peco-bin-${version}";
  src     = fetchurl {
    url = "https://github.com/peco/peco/releases/download/v0.5.1/peco_linux_amd64.tar.gz";
    sha256 = "0c8s8hn55wwg5mghn5d2lyid66p9706d2p2hdn9lf7k7mvbc5c3m";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp peco $out/bin/peco
    chmod +x $out/bin/peco
  '';
}

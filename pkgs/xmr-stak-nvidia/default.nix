{ stdenv, fetchurl, cmake, openssl, libmicrohttpd, cudatoolkit }:
stdenv.mkDerivation rec {
  version = "v1.1.0-1.4.0";
  name    = "xmr-stak-nvidia-${version}";

  src     = fetchurl {
    url     = "https://github.com/fireice-uk/xmr-stak-nvidia/archive/v1.1.1-1.4.0.tar.gz";
    sha256  = "16bf8c7g5vzl7aq6ns5iiilz1j5lrr2hcsyg7hllry4kzsdkp1i2";
  }; 

  buildInputs = [
    cmake openssl.dev libmicrohttpd cudatoolkit
  ];

  patchPhase = ''
    # I'd like to donate manually
    sed -i -e "s@1.0 / 100.0@0.0@" donate-level.h
  '';

  makeFlags = "PREFIX=$(out)";
  installPhase = "PREFIX=$out make install";

  cmakeFlagsArray = [
    "-DCUDA_ARCH=61"
  ];
}

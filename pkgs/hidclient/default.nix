{ stdenv, fetchFromGitHub, bluez, gcc, gnumake }:
stdenv.mkDerivation rec {
  version = "2012-07-28";
  name    = "hidclient-${version}";
  src     = fetchFromGitHub {
    owner   = "benizi";
    repo    = "hidclient";
    rev     = "master";
    sha256  = "01lwrja0pz86rn15g27k40yshq1n9cr977y64ffv8n1hwnjiz7qk";
  };

  buildInputs = [ bluez gcc gnumake ];

  buildPhase = ''
    make
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp hidclient $out/bin/hidclient
    chmod +x $out/bin/hidclient
  '';
}

{ stdenv, fetchurl }:
let
  fonts = [
    { name = "NotoSansCJKjp-Black"        ; sha256 = "1hxppqv29i5pdr00l563p1w20xjbhfmvmqkpxp2dv124c97hdg9h"; }
    { name = "NotoSansCJKjp-Bold"         ; sha256 = "0yisncln1japxpq7s0mxm73qpprrcwfddwfjq8mic5n66gq13acw"; }
    { name = "NotoSansCJKjp-DemiLight"    ; sha256 = "0ipffmkq5xs20maif4xxdrv1lc3xv2947ldwryccscn0g9l94xlc"; }
    { name = "NotoSansCJKjp-Light"        ; sha256 = "1b7vng7z98m4bw1jqam5zlhg9xrp9vrm2b7zhk8b80fyrhxyw9wa"; }
    { name = "NotoSansCJKjp-Medium"       ; sha256 = "17c3i91pwnx3g4bqb7rvriqnviwypyzh1s6szsj8xcms4b60pnmw"; }
    { name = "NotoSansCJKjp-Regular"      ; sha256 = "0gd2fr7nr7klyshm6zi6dnjd7ifln626vpyqjajqx34nznpm70pi"; }
    { name = "NotoSansCJKjp-Thin"         ; sha256 = "152s5732sjs1d1wn5p6zxs0sqfsrp2pfm7bxzd3l4j4p9d4j5lyn"; }

    { name = "NotoSansMonoCJKjp-Bold"     ; sha256 = "1p0q7i6cwq2lxjxgc33k5qx6hgmysd8kxl75zvlncdyszbvdssi0"; } 
    { name = "NotoSansMonoCJKjp-Regular"  ; sha256 = "0idz102snca7szm6ix2l7q6d6z82124rcrnf7slblpkxkqpwg5sb"; }

    { name = "NotoSerifCJKjp-Black"       ; sha256 = "0h26i7lc7xlgc98dh3lvsa0s4kxx4j9yyms0567yslg5b97ddlwr"; } 
    { name = "NotoSerifCJKjp-Bold"        ; sha256 = "1pwjfzh86d43nn5hwhshb5nhpcssnhih386alwzi7jzxxcbcpw32"; }
    { name = "NotoSerifCJKjp-ExtraLight"  ; sha256 = "0b4kcn55c6vbp1xa88d5ickf5i22nx873jzwkl7vcbar1i6fgca7"; }
    { name = "NotoSerifCJKjp-Light"       ; sha256 = "0ygvn20xqaplfzijsr7f4s5fr2szgvx79l5flqp90cyvgbg0bjl7"; }
    { name = "NotoSerifCJKjp-Medium"      ; sha256 = "0kv1453ic8g45bdmax1h9z00gqv5jsilidn35a8ca56yxsazqk00"; }
    { name = "NotoSerifCJKjp-Regular"     ; sha256 = "1ldp7abm93xrivpq1a075hvapsljj09q3b54v03a895c737m5886"; }
    { name = "NotoSerifCJKjp-SemiBold"    ; sha256 = "0q728b74qb66wmzrr4fk0d4lwi6hlw4c8vdpz2r6a1jfjdln3a13"; }
  ];
in
stdenv.mkDerivation rec {
  name    = "noto-fonts-cjk-jp-git";
  version = "git";

  files = map ({ name, sha256 }: fetchurl {
    url = "https://raw.githubusercontent.com/googlei18n/noto-cjk/master/${name}.otf";
    sha256 = "${sha256}";
  }) fonts;

  unpackPhase = ''
    mkdir -p noto

    ${stdenv.lib.strings.concatMapStrings (font: ''
      cp ${font} noto
    '') files}
  '';

  installPhase = ''
    mkdir -p $out/share/fonts/
    mv noto $out/share/fonts/
  '';

  meta = with stdenv.lib; {
    description = "Beautiful and free fonts for CJK languages";
    homepage = https://www.google.com/get/noto/help/cjk/;
    longDescription =
    ''
      Noto Sans CJK is a sans serif typeface designed as an intermediate style
      between the modern and traditional. It is intended to be a multi-purpose
      digital font for user interface designs, digital content, reading on laptops,
      mobile devices, and electronic books. Noto Sans CJK comprehensively covers
      Simplified Chinese, Traditional Chinese, Japanese, and Korean in a unified font
      family. It supports regional variants of ideographic characters for each of the
      four languages. In addition, it supports Japanese kana, vertical forms, and
      variant characters (itaiji); it supports Korean hangeul — both contemporary and
      archaic.
    '';
    license = licenses.ofl;
    platforms = platforms.all;
    maintainer = [ "nyarla <nyarla@thotp.net>" ];
  };
}

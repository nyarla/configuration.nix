{ stdenv, fetchurl, autoreconfHook, pkgconfig, gdbm }:

stdenv.mkDerivation rec {
  version = "1.3.3";
  name    = "skkdic-tools-${version}";

  src     = fetchurl {
    url = "https://github.com/skk-dev/skktools/archive/skktools-1_3_3.tar.gz";
    sha256 = "00inip72ly2yjld3hf9pv40hmvw3qqgznx1ivqd1ddlkmnflgzi1";
  };

  buildInputs = [
    autoreconfHook pkgconfig gdbm
  ];

  patchPhase = ''
    sed -i -e "s@gdbm/ndbm.h@ndbm.h@g" configure.ac
    sed -i -e "s@_gdbm_ndbm_@_ndbm_@g" configure.ac
   ''; 
}

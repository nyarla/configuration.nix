{ stdenv, fetchurl, gcc, gnumake, bluez, pkgconfig, perl }:
stdenv.mkDerivation rec {
  version = "v1.5";
  name    = "btkbdd-${version}";
  src     = fetchurl {
    url     = "https://github.com/lkundrak/btkbdd/archive/v1.5.tar.gz";
    sha256  = "1sm2f6xzdc652vqlypwwk4qcmyvy7vx3q6lx1kafabl2yqv98lgw";
  };

  buildInputs = [
    gcc gnumake bluez pkgconfig perl
  ];

  buildPhase = ''
    make
  '';

  installPhase = ''
    mkdir -p $out/
    make DESTDIR=$out PREFIX="" install
  '';
}

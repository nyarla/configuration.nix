{ stdenv, fetchurl }:
stdenv.mkDerivation rec {
  version = "v2.1.5";
  name    = "the-platinum-searcher-${version}";
  src     = fetchurl {
    url     = "https://github.com/monochromegane/the_platinum_searcher/releases/download/v2.1.5/pt_linux_amd64.tar.gz";
    sha256  = "1f8ajqxh16r2lsikg4r6lj32r5zal1gm1lpfqf30a8xa5k6qfhvd";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp pt $out/bin/.pt.real
    echo "$(< $NIX_CC/nix-support/dynamic-linker) $out/bin/.pt.real \"\$@\"" >"$out/bin/pt"
    chmod +x $out/bin/pt
    '';
}

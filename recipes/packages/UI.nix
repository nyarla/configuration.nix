{ pkgs, services, ... }:
let
  Xorg = {
    Xresources = pkgs.writeText "Xresources" ''
      Xft.dpi: 192
      Xft.antialias: 1
      Xft.hinting: 1
      Xft.autohint: 1
      Xft.hintstyle: hintfull
      Xft.lcdfilter: lcddefault
      Xcursor.size: 48
      Xcursor.theme: Paper
    '';

    DisplaySetupHelper = ''
      ${pkgs.xorg.xrdb}/bin/xrdb -merge ${Xorg.Xresources}
      ${pkgs.xorg.xsetroot}/bin/xsetroot -solid "#242424" -cursor_name arrow
      ${pkgs.xorg.xrandr}/bin/xrandr --output eDP-1 --dpi 192 
    '';

    XInit = ''
      ${pkgs.xorg.xrdb}/bin/xrdb -merge ${Xorg.Xresources}
      ${pkgs.xorg.xsetroot}/bin/xsetroot -solid "#242424" -cursor_name arrow
      ${pkgs.xorg.xrandr}/bin/xrandr --output eDP-1 --dpi 192 

      # GTK
      export GDK_SCALE=2
      export GDK_DPI_SCALE=0.5

      # Qt
      export QT_SCALE_FACTORS=2
      export QT_SCREEN_SCALE_FACTORS=1
      
    '';

    DisplayManager = {
      lightdm = {
        enable            = true;
        autoLogin.enable  = false;

        extraSeatDefaults = ''
          login-check-graphical=true
          allow-guest=false
          greeter-hide-users=true
          greeter-show-manual-login=true
          display-setup-script=${pkgs.writeScript "setup-helper" Xorg.DisplaySetupHelper}
        '';

        background = "#242424";

        greeters = {
          gtk = {
            enable = true;
            theme  = {
              name    = "Adapta";
              package = Packages.ThemeBundle;
            };

            iconTheme = {
              name    = "Paper";
              package = Packages.ThemeBundle;
            };

            # 17.03 is not supported extraConfig
            # extraConfig = Themes.GTKGreeter;
          };
        };
      };

      session = [
        { name = "icewm"; manage = "desktop"; start = ''
          ${Xorg.XInit}
          ${pkgs.icewm}/bin/icewm-session & export waitPID=$!
         ''; }
        { name = "console"; manage = "desktop"; start = ''
          ${Xorg.XInit}
          ${pkgs.mlterm}/bin/mlterm --geometry 256x67+0+0 \
                                    --type=xft \
                                    -e ${pkgs.wcwidth-cjk}/bin/wcwidth-cjk ${pkgs.zsh}/bin/zsh --login \
                                    & export waitPID=$!
          ''; }
      ];
    };
  };

  FontConfig = {
    enable      = true;
    dpi         = 192;
    antialias   = true;
    hinting     = {
      enable    = true;
      autohint  = true;
    };
    subpixel    = {
      lcdfilter = "default";
    };

    defaultFonts  = {
      sansSerif = [ "Noto Sans CJK JP"  ];
      serif     = [ "Noto Serif CJK JP" ];
      monospace = [ "RictyDiscord NF" "Noto Sans Mono CJK JP" "DejaVu Sans Mono"];
    };
  };

  Themes = {
    GTK3Settings = ''
      [Settings]
      gtk-theme-name=Adapta
      gtk-icon-theme-name=Paper
      gtk-cursor-theme-name=Paper
      gtk-cursor-theme-size=48
      gtk-font-name=Noto Sans CJK JP 10
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle=hintfull
    '';

    GTK2Settings = ''
      gtk-theme-name="Adapta"
      gtk-icon-theme-name="Paper"
      gtk-cursor-theme-name="Paper"
      gtk-font-name="Noto Sans CJK JP 10"
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle=hintfull
    '';

    GTKGreeter = ''
      gtk-icon-theme-name=Paper
      gtk-cursor-theme-name=Paper
      gtk-cursor-theme-size=48
      gtk-font-name=Noto Sans CJK JP 10
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle=hintfull
    '';

    ConsoleTheme = ''
      # this color palette is based on http://colors.smyck.org/
      ${pkgs.coreutils}/bin/echo -ne "\033]P0242424"
      ${pkgs.coreutils}/bin/echo -ne "\033]P1c75646"
      ${pkgs.coreutils}/bin/echo -ne "\033]P28eb33b"
      ${pkgs.coreutils}/bin/echo -ne "\033]P3d0b03c"
      ${pkgs.coreutils}/bin/echo -ne "\033]P472b3cc"
      ${pkgs.coreutils}/bin/echo -ne "\033]P5c8a0d1"
      ${pkgs.coreutils}/bin/echo -ne "\033]P6218693"
      ${pkgs.coreutils}/bin/echo -ne "\033]P7f7f7f7"
      ${pkgs.coreutils}/bin/echo -ne "\033]P85d5d5d"
      ${pkgs.coreutils}/bin/echo -ne "\033]P9e09690"
      ${pkgs.coreutils}/bin/echo -ne "\033]PAcdee69"
      ${pkgs.coreutils}/bin/echo -ne "\033]PBffe377"
      ${pkgs.coreutils}/bin/echo -ne "\033]PC9cd9f0"
      ${pkgs.coreutils}/bin/echo -ne "\033]PDfbb1f9"
      ${pkgs.coreutils}/bin/echo -ne "\033]PE77dfd8"
      ${pkgs.coreutils}/bin/echo -ne "\033]PFffffff"
    '';

    IconTheme = ''
      [Icon Theme]
      Inherits=Paper
    '';

    ThemeInit = ''
      rm -rf $HOME/.config/Trolltech.conf
      rm -rf $HOME/.config/gtk-3.0/settings.ini 

      # XDG
      export XDG_CONFIG_DIRS="/etc/xdg:$XDG_CONFIG_DIRS"
      export XDG_DATA_DIRS="${Packages.ThemeBundle}/share:/etc/xdg/share:$XDG_DATA_DIRS"
      
      export XDG_CONFIG_HOME=$HOME/.config
      export XDG_DATA_HOME=$HOME/.local/share
      export XDG_CACHE_HOME=$HOME/.cache

      # GTK
      export GTK_DATA_PREFIX="/run/current-system/sw"
      export GTK2_RC_FILES="/etc/gtk-2.0/gtkrc:${Packages.ThemeBundle}/share/themes/Adapta/gtk-2.0/gtkrc:$GTK2_RC_FILES"
      export GDK_PIXBUF_MODULE_FILE="$(echo ${pkgs.librsvg}/lib/gdk-pixbuf-2.0/*/loaders.cache)"

      # Qt
      export QT_QPA_PLATFORMTHEME=qt5ct
    '';
  };

  Packages = {
    icons = with pkgs; [
      paper-icon-theme gnome3.adwaita-icon-theme hicolor_icon_theme 
    ];

    widgets = with pkgs; [
      adapta-gtk-theme gtk-engine-murrine
    ];

    system = with pkgs; [
      libsForQt5.qtstyleplugins qt5ct
      gtk2-x11

      icewm compton hsetroot sxhkd
      polybar
    ];

    ThemeBundle = pkgs.stdenv.mkDerivation rec {
      version = "2017-05-01";
      name    = "theme-bundle-${version}";
      buildInputs = Packages.icons ++ Packages.widgets;

      unpackPhase = ''
        mkdir -p share/icons

        ${(pkgs.stdenv.lib.strings.concatMapStrings (pkg: ''
          for item in $(ls ${pkg.out}/share/icons) ; do
            if test $item != "." && test $item != ".." ; then
              ln -sf ${pkg.out}/share/icons/$item share/icons/$item
            fi
          done 
        '') Packages.icons)}

        mkdir -p share/themes
        ${(pkgs.stdenv.lib.strings.concatMapStrings (pkg: ''
          for item in $(ls ${pkg.out}/share/themes) ; do
            if test $item != "." && test $item != ".." ; then
              ln -sf ${pkg.out}/share/themes/$item share/themes/$item
            fi
          done 
        '') Packages.widgets)}
      '';
      
    installPhase = ''
      mkdir -p $out/
      mv share $out/
    '';
    };
  };
in
{
  # fonts
  # =====
  fonts = {
    enableFontDir = true;
    fontconfig    = FontConfig; 
    fonts         = with pkgs; [
      noto-fonts
      noto-fonts-cjk-jp
      noto-fonts-emoji # FIXME
    ];
  };

  # themes
  # ======
  environment.etc = {
    "xdg/gtk-3.0/settings.ini" = {
      text = Themes.GTK3Settings;
      mode = "444";
    };

    "gtk-2.0/gtkrc" = {
      text = Themes.GTK2Settings;
      mode = "444";
    };

    "xdg/share/icons/default/index.theme" = {
      text = Themes.IconTheme;
      mode = "444";
    };

    "xdg/share/icons/default/cursors" = {
      source = "${Packages.ThemeBundle}/share/icons/Paper/cursors";
    };

    "lightdm/lightdm-gtk-greeter.conf" = {
      text = Themes.GTKGreeter;
      mode = "444";
    };
  };

  # shell
  # =====
  environment.variables = {
    XCURSOR_PATH = [ "/etc/xdg/share/icons" ]; 
  };
  environment.profileRelativeEnvVars = {
    XCURSOR_PATH = [ "/etc/xdg/share/icons" ];
  };

  environment.extraInit = ''
    ${Themes.ConsoleTheme}
    ${Themes.ThemeInit}
  '';

  # display manager
  # ===============
  services.xserver.displayManager = Xorg.DisplayManager;

  # packages
  # ========
  environment.systemPackages = Packages.icons ++ Packages.widgets ++ Packages.system;
}

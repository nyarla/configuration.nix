{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    # terminal emulator
    # -----------------
    mlterm

    # editor
    # ------
    vimHugeX

    # web
    # ---
    vivaldi firefox 
    google-play-music-desktop-player

    # file mangement
    # --------------
    pcmanfm 

    # credential management
    # -------------------
    keepassx2
    gnome3.seahorse

    # utilities
    # ---------
    scrot xdo xdotool
    gnome3.gucharmap
    dunst libnotify
    tigervnc

    # wine
    # ----
    wine winetricks
  ];

  services.dbus.packages = with pkgs; [
    vivaldi firefox

    gnome3.dconf
    gnome3.gnome-disk-utility
  ];
}

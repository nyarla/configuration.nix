{ pkgs, ... }:
{
  security.wrappers = {
    fbterm = {
      source        = "${pkgs.fbterm.out}/bin/fbterm";
      capabilities  = "cap_sys_tty_config+ep";
    };
  };

  environment.systemPackages = with pkgs; [
    fbterm
    vim
  ];
}

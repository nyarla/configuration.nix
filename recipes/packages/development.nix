{ pkgs, boot, ... }:
{
  environment.systemPackages = with pkgs; [
    # shell
    # -----
    zsh tmux
    keychain wcwidth-cjk

    # console
    # -------
    ctags editorconfig-core-c peco-bin pt-bin

    # archiver
    # --------
    unzip zip unrar p7zip bzip2 gzip xz

    # version control
    # ---------------
    git mercurial bazaar subversion cvs

    # backup
    # ------
    bup rclone

    # compilers and toolchains
    # ------------------------
    gcc automake autoconf cmake gnumake

    # languages
    # ---------
    go glide
    nodejs yarn
    perl perlPackages.Appcpanminus perlPackages.Carton
  ];
}

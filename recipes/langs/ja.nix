{ pkgs, ... }:
{
  # input method
  # ------------
  i18n.inputMethod.enabled = "uim";

  # default locale
  i18n.defaultLocale = "ja_JP.UTF-8";
  environment.extraInit = ''
    export LANG=ja_JP.UTF-8
  '';


  # dictioinaries
  # -------------
  environment.systemPackages = with pkgs; [
    skkdic-tools

    # TODO; packaging
    #yaskkserv
    #skk-jisyo-xl
  ];
}

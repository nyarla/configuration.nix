{ pkgs, ... }:
{
  services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="backlight", RUN+="${pkgs.coreutils.out}/bin/chgrp video /sys/class/backlight/%k/brightness"
    ACTION=="add", SUBSYSTEM=="backlight", RUN+="${pkgs.coreutils.out}/bin/chmod g+w /sys/class/backlight/%k/brightness" 
  '';

  services.udev.packages = with pkgs; [
    coreutils
  ];

  environment.systemPackages = with pkgs; [
    light
  ];
}

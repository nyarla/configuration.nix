{ ... }:
{
  networking.firewall.enable                = true    ;
  networking.firewall.allowPing             = false   ;
  networking.firewall.allowedTCPPortRanges  = []      ;
  networking.firewall.allowedTCPPorts       = [
    5353
    9999
    55555
  ] ;
  networking.firewall.allowedUDPPortRanges  = [
  ] ;
  networking.firewall.allowedUDPPorts       = [
    5353
  ] ;

}

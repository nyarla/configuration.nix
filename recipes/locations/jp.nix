{ config, ... }:
{
  # timezone
  # --------

  time.timeZone = "Asia/Tokyo";

  # wireless restriction
  # --------------------
  boot.extraModprobeConfig = ''
    options cfg80211 ieee80211_regdom=JP
  '';
}

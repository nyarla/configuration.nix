{ ... }:
{
  # enable X.org services
  # ---------------------
  services.xserver.enable   = true;
  services.xserver.autorun  = true;

  # gnome keyring
  # -------------
  services.gnome3.gnome-keyring.enable = true;
    
  # avahi
  # -----
  services.avahi.enable   = true;
  services.avahi.nssmdns  = true;
  
  # cups
  # ----
  services.printing.enable = true;

  # dbus
  # ----
  services.dbus.enable = true;
  services.dbus.socketActivated = true;

  # resilio
  # -------
  services.resilio.enable = true;

  imports = [
    # softwares
    # ---------
    ../../recipes/packages/development.nix
    ../../recipes/packages/graphical.nix

    # interface
    # ---------
    ../../recipes/packages/UI.nix

    # security
    # --------
    ../../recipes/securities/firewall.nix

  ];
}

{ pkgs, ... }:
{
  # hostname
  # --------
  networking.hostName = "NyXPS15";
  networking.enableIPv6 = false;

  # accounts
  # --------
  users.mutableUsers = false;
  users.extraUsers.nyarla =
    { isNormalUser   = true;
      home           = "/home/nyarla";
      shell          = pkgs.zsh;
      description    = "Naoki OKAMURA";
      extraGroups    = [ "wheel" "video" "tty" "input" "vboxusers" ];
    };

  users.extraUsers.rslsync.extraGroups = [ "users" ];

  # nix channel
  # -----------
  system.stateVersion         = "17.03";
  system.autoUpgrade.channel  = "https://nixos.org/channels/nixos-17.03";
  system.autoUpgrade.enable   = false;

  # cups
  # ----
  services.printing.drivers = [ pkgs.gutenprint pkgs.cups-bjnp ];

  # autofs
  # ------
  services.autofs = {
    enable = true;
    autoMaster =
      let
        rules = pkgs.writeText "auto" ''
          backup -fstype=ext4,rw,noatime,nodiratime :/dev/disk/by-partuuid/b997798b-2a73-4999-8160-2d23934cf6d0
        '';
      in ''
        /data/mnt file:${rules}
      '';
  };

  # resilio sync
  # ------------
  services.resilio = {
    deviceName      = "NyXPS15";
    listeningPort   = 55555;
    httpListenAddr  = "127.0.0.1";
    httpListenPort  = 8888;
    enableWebUI     = true;
  };

  # features
  # --------
  imports = [
    # overrides
    ../nixpkgs/pkgs.nix 

    # hardware
    ../../hardware-configuration.nix
    ../hardwares/XPS-9560-JP.nix

    # devices
    ../../recipes/devices/backlight.nix

    # credentials
    ../../credentials/NyXPS15.nix

    # enviornment
    ../envs/desktop.nix
    ../envs/vmm.nix
    ../../recipes/langs/ja.nix
    ../../recipes/locations/jp.nix
  ];
 }

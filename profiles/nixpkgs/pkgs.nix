{ config, pkgs, ... }:
let
  use     = pkgs.lib.callPackageWith ( pkgs // pkgs.xorg // pkgs.gnome2 );
  require = name: opts: use "/etc/nixos/pkgs/${name}" opts;

in
{
  nixpkgs.config.allowUnfree      = true ;
  nixpkgs.config.packageOverrides = pkgs: with pkgs; {
    # console
    # -------
    skkdic-tools    = require "skkdic-tools"    { };
    wcwidth-cjk     = require "wcwidth-cjk"     { };
    
    peco-bin        = require "peco-bin"        { };
    pt-bin          = require "pt-bin"          { };

    btkbdd          = require "btkbdd"          { };
    hidclient       = require "hidclient"       { };
    xmr-stak-nvidia = require "xmr-stak-nvidia" { };

    # fonts
    # -----
    noto-fonts-cjk-jp = require "noto-fonts-cjk-jp" { };

    # terminal emulator
    # -----------------
    fbterm  = pkgs.fbterm.overrideAttrs (old: rec {
      patches = [ "${prefix}/pkgs/fbterm/patches/color.patch" ];
    });

    mlterm  = mlterm.overrideAttrs (old: rec {
      buildInputs = old.buildInputs ++ [ uim ];
      configureFlags = old.configureFlags ++ [
        "--enable-antialias"
      ];
    });

    # utilities
    # ---------
    synergy = use "${fetchurl {
      url     = "https://raw.githubusercontent.com/NixOS/nixpkgs/master/pkgs/applications/misc/synergy/default.nix";
      sha256  = "1k5hd3xlkyv1ncpgzznqc79ls5d7m9k3gjsvk65mqh6bvz93gj1b";
    }}" { };

    # web
    # ---    
    google-play-music-desktop-player = use "${fetchurl {
      url = "https://raw.github.com/NixOS/nixpkgs/master/pkgs/applications/audio/google-play-music-desktop-player/default.nix";
      sha256 = "19ys9cjslk4yn8kz13ldf55ccxm5yk12z1gj39z6vfv3233kc15h";
    }}" { };

    vivaldi = (use "${fetchurl {
      url = "https://raw.githubusercontent.com/NixOS/nixpkgs/master/pkgs/applications/networking/browsers/vivaldi/default.nix";
      sha256 = "0c5926smzn8gg3pzl57cqji152v19jbyfb9rv0ywyyx7gxgfa4xg";
    }}" { }).overrideAttrs (old: rec {
      buildInputs   = old.buildInputs ++ [ makeWrapper wrapGAppsHook gnome3.dconf ];
      installPhase  = old.installPhase + ''
        rm $out/bin/vivaldi
        eval makeWrapper $out/opt/vivaldi/vivaldi $out/bin/vivaldi ${
          stdenv.lib.concatMapStringsSep " " (plugin: "$(< \"${plugin}/nix-support/wrapper-flags\")") (callPackage <nixpkgs/pkgs/applications/networking/browsers/chromium/plugins.nix> {
            enableWideVine    = false;
            enablePepperFlash = true;
            upstream-info     = import <nixpkgs/pkgs/applications/networking/browsers/chromium/upstream-info.nix>;
          }).enabled
        }
      '';
    });

    # vmm
    # ---
    virtualbox = virtualbox.override {
      enableExtensionPack = true;
    };

    # X.org
    # -----
    polybar = polybar.override {
      i3GapsSupport =  true;
    };

    # Wayland
    # -------
    wlc = wlc.overrideAttrs (old: rec {
      version = "git";
      src     = fetchgit {
        url     = "https://github.com/Cloudef/wlc";
        rev     = "refs/heads/master";
        sha256  = "";
        fetchSubmodules = true;
      };

      natvieBuildInputs = old.nativeBuildInputs ++ [ 
        xorg.libxcb.dev
        xorg.libXau.dev
        xorg.libXdmcp.dev
        xorg.libXext.dev
        xorg.libXxf86vm.dev
        gdk_pixbuf.dev
        xorg.libpthreadstubs
      ];

      cmakeFlagsArray = [
        "-DCMAKE_BUILD_TYPE=Upstream"
        "-DWLC_BUILD_STATIC=off"
        "-DWLC_BUILD_EXAMPLES=off"
        "-DWLC_BUILD_TESTS=off"
      ];
    });

    sway = sway.overrideAttrs (old: rec {
      version = "git";

      src     = fetchgit {
        url     = "https://github.com/SirCmpwn/sway";
        rev     = "refs/heads/master";
        sha256  = "";
      };

      nativeBuildInputs = old.nativeBuildInputs ++ [
        xorg.libxcb.dev
        xorg.libXau.dev
        xorg.libXdmcp.dev
        xorg.libXext.dev
        xorg.libXxf86vm.dev
        xorg.libpthreadstubs
        gdk_pixbuf.dev
        pam
      ];

      cmakeFlagsArray = [
        "-DCMAKE_BUILD_TYPE=Upstream"
      ];
    });

    # libraries
    # ---------
    qt48Full = qt48Full.override {
      gtkStyle = true;
    };

    perlPackages  = let self = _self // perlPackages; _self = with self; {
      Carton = buildPerlPackage rec {
        version = "v1.0.28";
        name    = "carton-${version}";
        src     = fetchurl {
          url     = "mirror://cpan/authors/id/M/MI/MIYAGAWA/Carton-${version}.tar.gz";
          sha256  = "0gjswy424z22c8cw93x6wlpmvb6niqkajs91b76a20scc05pjndw";
        };
        propagatedBuildInputs = [
          JSON ModuleMetadata ModuleCPANfile TryTiny Parent GetoptLong ClassTiny PathTiny
          Appcpanminus CPANMeta CPANMetaRequirements ModuleCoreList ModuleReader
        ];
      };

      ClassTiny = buildPerlPackage rec {
        version = "1.006";
        name    = "Class-Tiny-${version}";
        src     = fetchurl {
          url = mirror://cpan/authors/id/D/DA/DAGOLDEN/Class-Tiny-1.006.tar.gz;
          sha256 = "0knbi1agcfc9d7fca0szvxr6335pb22pc5n648q1vrcba8qvvz1f";
        };

        propagatedBuildInputs = [ Carp ];
      };

      ModuleCPANfile = buildPerlPackage rec {
        version = "1.1002";
        name    = "Module-CPANfile-${version}";
        src     = fetchurl {
          url = mirror://cpan/authors/id/M/MI/MIYAGAWA/Module-CPANfile-1.1002.tar.gz;
          sha256 = "1z9wsps70h7ypaxmwq8l8wp0dg4kqrxmaqgzbh2fq8jw9idw55dz";
        };
        propagatedBuildInputs = [ CPANMeta JSON Parent ];
      };

      ModuleReader = buildPerlPackage rec {
        version = "0.003002";
        name    = "Module-Reader-${version}";
        src     = fetchurl {
          url = mirror://cpan/authors/id/H/HA/HAARG/Module-Reader-0.003002.tar.gz;
          sha256 = "1pn0q5qh9jy5jy2bbvjpyv3m5j11qvsf8q8arzaga009g6a9szd6";
        };
        propagatedBuildInputs = [ ScalarListUtils ];
      };
    }; in self;

    # wine
    # ----
    winetricks = winetricks.overrideAttrs (old: rec {
      src = fetchFromGitHub {
        owner   = "Winetricks";
        repo    = "winetricks";
        rev     = "20170517";
        sha256  = "1av36c5zs5rc0ab4g2h4i6g19c9xs7si18lgn431hdnqh7hw086h";
      };
    });

    wine = (use <nixpkgs/pkgs/misc/emulators/wine/base.nix> rec {
      version = "2.9";
      name = "wine-wow-${version}";
      src = fetchurl {
        url = "https://dl.winehq.org/wine/source/2.x/wine-${version}.tar.xz";
        sha256 = "0pslhs3kwjimlz5ad6vzzdw7lgbfpwl8vyg4jmf17p61pfxzfimc";
      };
      stdenv = pkgs.stdenv_32bit;
      pkgArches = [
        pkgs pkgsi686Linux
      ];
      geckos  = [
        (fetchurl rec {
          url = "http://dl.winehq.org/wine/wine-gecko/2.47/wine_gecko-2.47-x86.msi";
          sha256 = "0fk4fwb4ym8xn0i5jv5r5y198jbpka24xmxgr8hjv5b3blgkd2iv";
        }) 
        (fetchurl rec {
          url = "http://dl.winehq.org/wine/wine-gecko/2.47/wine_gecko-2.47-x86_64.msi";
          sha256 = "0zaagqsji6zaag92fqwlasjs8v9hwjci5c2agn9m7a8fwljylrf5";
        })
      ];
      monos   = [
        (fetchurl rec {
          url = "http://dl.winehq.org/wine/wine-mono/4.6.4/wine-mono-4.6.4.msi";
          sha256 = "0lj1rhp9s8aaxd6764mfvnyswwalafaanz80vxg3badrfy0xbdwi";
        })
      ];
      buildScript = <nixpkgs/pkgs/misc/emulators/wine/builder-wow.sh>;
      platforms = [ "x86_64-linux" ];
      supportFlags = {
        pngSupport        = true;
        jpegSupport       = true;
        tiffSupport       = true;
        gettextSupport    = true;
        fontconfigSupport = true;
        alsaSupport       = true;
        openglSupport     = true;
        tlsSupport        = true;
        gstreamerSupport  = true;
        cupsSupport       = true;
        dbusSupport       = true;
        mpg123Support     = true;
        openalSupport     = true;
        odbcSupport       = true;
        netapiSupport     = true;
        cursesSupport     = true;
        vaSupport         = true;
        v4lSupport        = true;
        saneSupport       = true;
        gsmSupport        = true;
        gphoto2Support    = true;
        pulseaudioSupport = true;
        xineramaSupport   = true;
        xmlSupport        = true;
        cairoSupport      = true;
        pcapSupport       = true;
        openclSupport     = true;

        ldapSupport       = false;
        gtkSupport        = false;
        colorManagementSupport = false;
      };
    });
  };
}


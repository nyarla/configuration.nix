{ pkgs, ... }:
{
  # bootloader
  # ----------
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # kernel
  boot.kernelPackages = pkgs.linuxPackages_4_11;
  boot.kernelParams = [
    "acpi_rev_override=5"
  ];

  hardware.cpu.intel.updateMicrocode = true;

  # video drivers
  # -------------
  services.xserver.videoDrivers = [
    "i965"
  ];

  hardware.opengl.enable = true;
  hardware.opengl.driSupport = true;
  hardware.opengl.driSupport32Bit = true;
  
  hardware.bumblebee.enable = true;
  hardware.bumblebee.driver = "nvidia";

  environment.systemPackages = with pkgs; [
    primus cudatoolkit
  ];

  services.xserver.deviceSection = ''
    Option "TearFree" "true"
  '';

  # input drivers
  # -------------
  services.xserver.libinput = {
    enable            = true;
    naturalScrolling  = true;
  };

  # linux console
  # -------------
  i18n.consoleFont    = "${pkgs.terminus_font}/share/consolefonts/ter-v32n.psf.gz";
  i18n.consoleKeyMap  = "/etc/nixos/assets/keymap/XPS-9560-JP";

  # X.org
  # -----
  services.xserver.xrandrHeads = [
    "eDP-1"
  ];

  services.xserver.layout     = "jp";
  services.xserver.xkbModel   = "jp106";
  services.xserver.xkbOptions = "ctrl:nocaps";

  # wireless
  # --------
  networking.wireless.enable = true;
}
